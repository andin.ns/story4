from django.urls import path
from . import views

app_name = 'story4menangis'

urlpatterns = [
    path('', views.home, name='home'),
    path('about/', views.about, name='about'),
    path('portfolio/', views.portfolio, name='portfolio'),
    path('playlist/', views.playlist, name='playlist'),
    path('schedule/', views.schedule, name='schedule'),
    path('schedule/create/', views.schedule_create, name='schedule_create'),
    path('schedule/delete/', views.schedule_delete, name='schedule_delete'),
    path('schedule/deletespesifik/<id>', views.schedule_deletespesifik, name='schedule_deletespesifik')
]
