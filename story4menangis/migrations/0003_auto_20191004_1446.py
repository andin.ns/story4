# Generated by Django 2.2.6 on 2019-10-04 07:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('story4menangis', '0002_auto_20191003_2307'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='schedule',
            name='created_date',
        ),
        migrations.RemoveField(
            model_name='schedule',
            name='published_date',
        ),
        migrations.AddField(
            model_name='schedule',
            name='category',
            field=models.CharField(choices=[('PERSONAL', 'Personal'), ('SCHOOL', 'School'), ('BUSINESS', 'Business')], default='', max_length=1),
        ),
        migrations.AddField(
            model_name='schedule',
            name='date',
            field=models.DateField(default=''),
        ),
        migrations.AddField(
            model_name='schedule',
            name='day',
            field=models.CharField(default='', max_length=10),
        ),
        migrations.AddField(
            model_name='schedule',
            name='location',
            field=models.TextField(default=''),
        ),
        migrations.AddField(
            model_name='schedule',
            name='time',
            field=models.TimeField(default=''),
        ),
        migrations.AlterField(
            model_name='schedule',
            name='email',
            field=models.EmailField(default='', max_length=254),
        ),
        migrations.AlterField(
            model_name='schedule',
            name='title',
            field=models.TextField(default=''),
        ),
    ]
