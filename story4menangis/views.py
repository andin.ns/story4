from django.shortcuts import render, redirect
from .models import Schedule
from . import forms
from django.http import HttpResponse

# Create your views here.
def home(request):
    return render(request, 'home.html')

def about(request):
    return render(request, 'about.html')

def portfolio(request):
    return render(request, 'portfolio.html')

def playlist(request):
    return render(request, 'playlist.html')

def schedule(request):
    schedules = Schedule.objects.all().order_by('date')
    return render(request, 'schedule.html', {'schedule': schedules})

def schedule_create(request):
    if request.method == 'POST':
        form = forms.ScheduleForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/schedule/')
    else:
        form = forms.ScheduleForm()
    return render(request, 'schedule_create.html', {'form': form})

def schedule_delete(request):
    Schedule.objects.all().delete()
    return render(request, "schedule.html")

def schedule_deletespesifik(request, id):
    schedule = Schedule.objects.get(id=id)
    schedule.delete()
    return redirect('/schedule/')
