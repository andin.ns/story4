# Generated by Django 2.2.6 on 2019-10-08 13:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('story4menangis', '0014_auto_20191008_2009'),
    ]

    operations = [
        migrations.AlterField(
            model_name='schedule',
            name='date',
            field=models.DateField(null=True),
        ),
        migrations.AlterField(
            model_name='schedule',
            name='time',
            field=models.TimeField(null=True),
        ),
    ]
