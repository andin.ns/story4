from django.db import models
from datetime import datetime

# Create your models here.
CATEGORY_CHOICES = (
    ('PERSONAL', 'Personal'),
    ('SCHOOL', 'School'),
    ('BUSINESS', 'Business'),
)

class Schedule(models.Model):
    name = models.CharField(max_length=100, default='')
    title = models.TextField(max_length=100)
    category = models.CharField(max_length=100, choices=CATEGORY_CHOICES)
    location = models.TextField(max_length=30)
    date = models.DateField()
    time= models.TimeField()