# Generated by Django 2.2.6 on 2019-10-04 07:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('story4menangis', '0003_auto_20191004_1446'),
    ]

    operations = [
        migrations.AlterField(
            model_name='schedule',
            name='date',
            field=models.DateField(default='dd-mm-yyyy'),
        ),
    ]
